#include <stdio.h>
#include <string.h>

void reverseString(char *str) 
{
    int len = strlen(str);
    char *start = str;
    char *end = str + len - 1;
    char temp;
    int i = 0;
 /*  Con arreglos */
    for (i = 0; i < len/2; i++)
    {
        temp = str[i];
        str[i] = str[len - i - 1];
        str[len - i - 1 ] = temp;
    }
/* Con punteros */
/*
    while (start < end) 
    {
        temp = *start;
        *start = *end;
        *end = temp;
        start++;
        end--;
    }
*/
}

int main() {
    char str[101];  // 100 caracteres + null terminator
    printf("\n Ingrese un string: ");
    fgets(str, 101, stdin);

    reverseString(str);
    printf("\n String inverso: %s\n", str);

    return 0;
}
