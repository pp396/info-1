## Parte 1

### Ejercicio 1

Realice una función que recibe un string del usuario, lo invierte e imprime el resultado. Maximo 100 caracteres

### Ejercicio 2

Realice una función que recibe dos matrices de 2x2 y una tercera matriz donde guardará el resultado del producto de ambas.

### Ejercicio 3

Ingrese 10 valores numéricos que representarán muestras de presión provenientes de un tanque presurizado. Luego, imprimir el mínimo y el máximo de la serie. Determinar si el promedio de los valores es más cercano al mínimo o al máximo y la diferencia.
Finalmente ordenar los valores de mayor a menor

### Ejercicio 4

Realizar un programa en el que se ingresan los nombres de 10 alumnos con sus promedios anuales. El programa imprime la lista de alumnos en orden de mérito separando los alumnos en grupos de notas: (0,4) , [4,7) y [7,10].

### Ejericico 5

Realizar un programa en el que se ingresan los nombres de 10 alumnos. A continuación, se pide ingresar un nombre. Si este último está en la lista, el programa indica en qué posición se ingresó o su inexistencia.

### Ejercicio 6

Una nave espacial proveniente del planeta Hoth está atacando a la tierra, la única esperanza para la humanidad está en activar las defensas interplanetarias.
Para esto un agente del más alto rango en las fuerzas debe ingresar un código secreto de 4 dígitos(A, B, C, D) por teclado. Dicho código tiene las siguientes características:

1. No tiene números repetidos
2. La suma de cada dígito da 23
3. (A*B) - D + C = 8
4. D - A - B - C = -5
5. A - B + C + D = 13

Escriba un programa que pueda recibir el código ingresado por teclado e imprima un mensaje de éxito en caso de que el código sea correcto.
Pero mucho cuidado, quien ingresa el código puede ser un enemigo por lo que solo tendrá 3 intentos. En caso de fallar se imprimirá una alerta por pantalla
